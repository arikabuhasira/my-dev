

alias ls='ls --color=auto'
alias ll='ls -hla --color=auto'
alias la='ls -a --color=auto'

# Misc
alias g='grep -i'  # Case insensitive grep
alias f='find . -iname'
alias ducks='du -cksh * | sort -rn|head -11' # Lists folders and files sizes in the current folder
alias systail='tail -f /var/log/system.log'
alias m='more'
alias df='df -h'
alias pip='pip3'


alias workon_py38="source $HOME/.venv/bin/activate"
