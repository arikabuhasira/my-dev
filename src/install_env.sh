#! /bin/bash 

dir=$(pwd)


echo "-------------------------------------------------"
echo "Install dev environment"
echo "-------------------------------------------------"
echo
echo

if [ -f /usr/bin/lsb_release ]; then
	sudo sed  -ri  's/^(\#\!\/)(.+)python(.+)/\1\2python3.5 -Es/' /usr/bin/lsb_release
fi

echo ">>> Install bashrc <<<"
cd $dir
cp .bash* $HOME/
cp .gitconfig $HOME/

echo 'source $HOME/.bash_aliases' >> $HOME/.bashrc
echo 'source $HOME/.bash_git' >> $HOME/.bashrc 
echo 'source $HOME/.bash_functions' >> $HOME/.bashrc 
echo 'source $HOME/.bash_prompt' >> $HOME/.bashrc 

echo ">>> Install Packages <<<"
source install_packages.sh
