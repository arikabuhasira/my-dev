#!/bin/bash
#
# $file: install_python_38.sh
# $author: ArikA (arikabuhasira@gmail.com) 
#
# This script download, build and install python3.8


sudo apt-get install -y \
	   make             \
	   build-essential  \
	   libssl-dev       \
	   zlib1g-dev       \
	   libbz2-dev       \
       libreadline-dev  \
	   libsqlite3-dev   \
	   wget             \
	   curl             \
	   llvm             \
	   libncurses5-dev  \
	   libncursesw5-dev \
       xz-utils         \
	   tk-dev           \
	   libffi-dev       \
	   liblzma-dev      \
	   python-openssl

echo "About to install python3.8 in /opt"
cd /opt
sudo wget https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz
sudo tar xvf Python-3.8.1.tgz

echo "Build Python 3.8"
cd Python-3.8.1
sudo ./configure --enable-optimizations
sudo make
sudo make altinstall


echo "Create virtual env"
python3.8 -m venv $HOME/.venv 


