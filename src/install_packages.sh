#!/bin/bash
#
# $file: install_packages.sh 
# $author: ArikA (arikabuhasira@gmail.com) 
#
# The script install mandatory apt packages for MY developer machine
#

packages=(
	'git'
	'bc'
	'apt-transport-https'
	'ca-certificates'
	'curl'
	'gnupg2'
	'software-properties-common'
	'build-essential'
	'autoconf'
	'automake'
	'gdb'
	'wget'
	'vim-nox'
	'libncurses5-dev' 
	'libncursesw5-dev'
	'python3-dev'
	'python3-pip'
	'cmake'
	)


function install_packages {
	for i in "${packages[@]}";do 
		echo "Installing packages $i"
		sudo apt-get install -y  ${i}
	done
}

function install_go {
	wget https://dl.google.com/go/go1.13.4.linux-amd64.tar.gz
	sudo tar -C /usr/local -xzf go1.13.4.linux-amd64.tar.gz
	echo 'PATH=$PATH:/usr/local/go/bin' >> $HOME/.bashrc
}

function install_fzf {
	fzf_dir=$HOME/.fzf
	git clone --depth 1 https://github.com/junegunn/fzf.git $fzf_dir
	pushd $fzf_dir
	./install
	popd
}

sudo apt-get update
install_packages
install_go
install_fzf
