#!/bin/bash
#
# $file: install_vim.sh
# $author: ArikA (arikabuhasira@gmail.com) 
#
# Install VIM recomended plugins 

current_dir=$(pwd)

function compile_vim {
	echo "Compiling VIM  with full features (+python, +ruby)"
	pushd /tmp
	rm -rf vim 
	git clone https://github.com/vim/vim.git  vim

	pushd vim
	./configure --with-features=huge \
		--enable-multibyte \
		--enable-rubyinterp \
		--enable-python3interp \
		--with-python3-config-dir=/usr/lib/python3/config \
		--enable-perlinterp \
		--enable-luainterp \
		--enable-gui=gtk2 \
		--enable-cscope \
		--prefix=/usr
	make VIMRUNTIMEDIR=/usr/share/vim/vim82
	sudo make install

	popd
	popd
}

function config_vim {
	echo "Setup vimrc"
	cp .vimrc  $HOME/

	echo Setup vim RUNTIME
	cp .vimruntime $HOME/
	echo 'source $HOME/.vimruntime' >> $HOME/.bashrc 
}

function install_plugins {
	echo "Init .vim"

	autoload_dir=$HOME/.vim/autoload
	bundle_dir=$HOME/.vim/bundle

	mkdir -p $autoload_dir
	mkdir -p $bundle_dir

	echo "Installing Pathogen"
	curl -LSso $autoload_dir/pathogen.vim https://tpo.pe/pathogen.vim

	packages=(
		'kien/ctrlp.vim.git'
		'vim-airline/vim-airline.git'
		'flazz/vim-colorschemes'
		'vim-airline/vim-airline-themes' 
		'scrooloose/nerdtree' 
		'majutsushi/tagbar' 
		'plasticboy/vim-markdown'
		'MattesGroeger/vim-bookmarks'
		'python-mode/python-mode'
		'ycm-core/YouCompleteMe'
		'junegunn/fzf'
		'junegunn/fzf.vim'
	)

	pushd $bundle_dir

	for i in "${packages[@]}";do 
		pkg_name=$(sed -r 's/^(.+)\/([^\/]+)$/\2/g' <<< $i)
		pkg_name=$(sed -r 's/^(.+)\.git$/\1/g' <<< $pkg_name)
		rm -Rf $pkg_name 
		echo "Installing packages $i"
		git clone --recurse-submodules "https://github.com/$i"   > /dev/null 2>&1
	done

	popd
}


function install_youcompleteme {
	youcompleteme_dir=$HOME/.vim/bundle/YouCompleteMe

	echo "Setup YouCompleteMe at $youcompleteme_dir"
	cp .ycm_extra_conf.py $youcompleteme_dir
	pushd $youcompleteme_dir
	python3 install.py --clang-completer
	popd
}


compile_vim
config_vim
install_plugins
install_youcompleteme

