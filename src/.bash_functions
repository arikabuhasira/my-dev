#
#  $file: .bash_functions 
#  $author: ArikA (arikabuhasira@gmail.com)


function git_branch {
	git branch -vv 2>/dev/null |          \
       awk '/^\*/ {a-0; b=0;              \
	   if ($5 == "ahead") a=$6;           \
	   if ($5 == "behind") b=$6;          \
	   if ($7 == "behind") b=$8;          \
	   printf ("-(%s+%d-%d)",$2,a,b);}'
}
